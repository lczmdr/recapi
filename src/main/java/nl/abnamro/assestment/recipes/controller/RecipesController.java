package nl.abnamro.assestment.recipes.controller;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import nl.abnamro.assestment.recipes.exceptions.ServiceError;
import nl.abnamro.assestment.recipes.model.request.RecipeRequest;
import nl.abnamro.assestment.recipes.model.search.SearchQuery;
import nl.abnamro.assestment.recipes.service.RecipesService;

import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/recipes")
@RequiredArgsConstructor
public class RecipesController {

    private final RecipesService recipesService;

    @Operation(
            operationId = "create-recipe",
            summary = "Create a new recipe",
            responses = {
                    @ApiResponse(responseCode = "201", description = "New Recipe Created", content = { @Content(mediaType = APPLICATION_JSON_VALUE, schema = @Schema(implementation = RecipeRequest.class)) }),
                    @ApiResponse(responseCode = "400", description = "Invalid Input", content = { @Content(mediaType = APPLICATION_JSON_VALUE, schema = @Schema(implementation = ServiceError.class)) }),
                    @ApiResponse(responseCode = "404", description = "Invalid Input", content = { @Content(mediaType = APPLICATION_JSON_VALUE, schema = @Schema(implementation = ServiceError.class)) })
            }
    )
    @RequestMapping(method = RequestMethod.POST, value = "", produces = { APPLICATION_JSON_VALUE }, consumes = { APPLICATION_JSON_VALUE })
    public ResponseEntity<RecipeRequest> createRecipe(@Parameter(name = "RecipeRequest") @RequestBody(required = false) RecipeRequest recipeRequest) {
        return new ResponseEntity<>(recipesService.create(recipeRequest), HttpStatus.CREATED);
    }

    @Operation(
            operationId = "get-all-recipes",
            summary = "Gets all the recipes",
            responses = { @ApiResponse(responseCode = "200", description = "All paginated recipes fetched") }
    )
    @RequestMapping(method = RequestMethod.GET, value = "", produces = { APPLICATION_JSON_VALUE })
    public ResponseEntity<Page<RecipeRequest>> getAllRecipes(@Parameter(name = "pageSize", example = "10") @RequestParam(required = false) Integer pageSize, @Parameter(name = "pageNumber", example = "0") @RequestParam(required = false) Integer pageNumber) {
        return new ResponseEntity<>(recipesService.getAll(pageSize, pageNumber), HttpStatus.OK);
    }

    @Operation(
            operationId = "get-recipe",
            summary = "Gets existing recipe",
            responses = { @ApiResponse(responseCode = "200", description = "recipe fetched", content = { @Content(mediaType = APPLICATION_JSON_VALUE, schema = @Schema(implementation = RecipeRequest.class)) }) }
    )
    @RequestMapping(method = RequestMethod.GET, value = "/{recipeId}", produces = { APPLICATION_JSON_VALUE })
    public ResponseEntity<RecipeRequest> getRecipe(@Parameter(name = "recipeId", description = "recipe id", required = true) @PathVariable("recipeId") Long recipeId) {
        return new ResponseEntity<>(recipesService.getRecipe(recipeId), HttpStatus.OK);
    }

    @Operation(
            operationId = "update-recipe",
            summary = "Update existing recipe",
            responses = { @ApiResponse(responseCode = "200", description = "recipe updated", content = { @Content(mediaType = APPLICATION_JSON_VALUE, schema = @Schema(implementation = RecipeRequest.class)) }) }
    )
    @RequestMapping(method = RequestMethod.PUT, value = "/{recipeId}", produces = { APPLICATION_JSON_VALUE }, consumes = { APPLICATION_JSON_VALUE })
    public ResponseEntity<RecipeRequest> update(@Parameter(name = "recipeId", description = "recipe id", required = true) @PathVariable("recipeId") Long recipeId,
            @Parameter(name = "RecipeRequest") @RequestBody(required = false) RecipeRequest recipeRequest) {
        return new ResponseEntity<>(recipesService.updateRecipe(recipeRequest, recipeId), HttpStatus.OK);
    }

    @Operation(
            operationId = "delete-recipe",
            summary = "Delete existing recipe",
            responses = { @ApiResponse(responseCode = "200", description = "recipe deleted") }
    )
    @RequestMapping(method = RequestMethod.DELETE, value = "/{recipeId}", produces = { "application/json" })
    public ResponseEntity deleteRecipe(@Parameter(name = "recipeId", description = "recipe id", required = true) @PathVariable("recipeId") Long recipeId) {
        recipesService.delete(recipeId);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @Operation(
            operationId = "search-recipe",
            summary = "Search Recipe",
            responses = { @ApiResponse(responseCode = "200", description = "recipes search using criteria") }
    )
    @RequestMapping(method = RequestMethod.POST, value = "/search", produces = { APPLICATION_JSON_VALUE }, consumes = { APPLICATION_JSON_VALUE })
    public ResponseEntity<Page<RecipeRequest>> search(@RequestBody SearchQuery searchRecipeRequest){
        return new ResponseEntity<>(recipesService.search(searchRecipeRequest), HttpStatus.OK);
    }

}
