package nl.abnamro.assestment.recipes.repositories;

import nl.abnamro.assestment.recipes.model.entity.Recipe;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface RecipesRepository extends JpaRepository<Recipe, Long>, JpaSpecificationExecutor<Recipe> {

}
