package nl.abnamro.assestment.recipes.repositories;

import nl.abnamro.assestment.recipes.model.entity.Ingredient;

import org.springframework.data.jpa.repository.JpaRepository;

public interface IngredientsRepository extends JpaRepository<Ingredient, Long> {

    long deleteByRecipeId(Long recipeId);
}
