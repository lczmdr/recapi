package nl.abnamro.assestment.recipes.service;

import java.util.List;
import java.util.Set;

import javax.persistence.EntityNotFoundException;

import nl.abnamro.assestment.recipes.exceptions.RecipeNotFoundException;
import nl.abnamro.assestment.recipes.model.entity.Ingredient;
import nl.abnamro.assestment.recipes.model.entity.Recipe;
import nl.abnamro.assestment.recipes.model.request.RecipeRequest;
import nl.abnamro.assestment.recipes.model.search.SearchQuery;
import nl.abnamro.assestment.recipes.repositories.IngredientsRepository;
import nl.abnamro.assestment.recipes.repositories.RecipesRepository;
import nl.abnamro.assestment.recipes.search.SpecificationSearch;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class RecipesService {

    private final ObjectMapper objectMapper;

    private final RecipesRepository recipesRepository;

    private final IngredientsRepository ingredientsRepository;

    /**
     * Creates a new recipe based in the request.
     * @param recipeRequest the request
     * @return the persisted request
     */
    public RecipeRequest create(RecipeRequest recipeRequest) {
        final var recipe = mapTo(recipeRequest);
        if (!CollectionUtils.isEmpty(recipe.getIngredients())) {
            recipe.getIngredients().forEach(i -> i.setRecipe(recipe));
        }
        final var storedRecipe = recipesRepository.save(recipe);
        return mapTo(storedRecipe);
    }

    /**
     * Gets all the recipes using the pagination filter.
     * @param pageSize the page size
     * @param pageNumber the page number
     * @return the fetched recipes
     */
    public Page<RecipeRequest> getAll(int pageSize, int pageNumber) {
        return recipesRepository.findAll(PageRequest.of(pageNumber, pageSize == 0 ? 10 : pageSize)).map(this::mapTo);
    }

    /**
     * Gets an specific recipe using the ID
     * @param recipeId the recipe ID
     * @return the found recipe, otherwise it throws an exception
     */
    public RecipeRequest getRecipe(Long recipeId) {
        final var recipeWithId = findRecipeWithId(recipeId);
        return mapTo(recipeWithId);
    }

    /**
     * Find a recipe with a specific ID
     * @param recipeId the recipe ID
     * @return the fetched recipe.
     */
    private Recipe findRecipeWithId(Long recipeId) {
        return recipesRepository.findById(recipeId).orElseThrow(() -> new RecipeNotFoundException("Could not find recipe with id: " + recipeId));
    }

    /**
     * Updates the existing recipe, if found
     * @param recipeRequest the recipe request with new values
     * @param recipeId the recipe id to be updated
     * @return the updated recipe
     */
    public RecipeRequest updateRecipe(RecipeRequest recipeRequest, Long recipeId) {
        findRecipeWithId(recipeId);
        final Recipe recipe = mapTo(recipeRequest);
        recipe.setId(recipeId);
        if (!CollectionUtils.isEmpty(recipe.getIngredients())) {
            recipe.getIngredients().forEach(i -> i.setRecipe(recipe));
        }
        ingredientsRepository.deleteByRecipeId(recipeId);
        final var updatedRecipe = recipesRepository.save(recipe);
        return mapTo(updatedRecipe);
    }

    /**
     * Deletes existing recipe, if found.
     * @param recipeId the recipe id to be deleted
     */
    public void delete(Long recipeId){
        Recipe recipeEntity = findRecipeWithId(recipeId);
        recipesRepository.delete(recipeEntity);
    }

    /**
     * Searchs recipes based in a criteria query.
     * @param searchQuery the search query
     * @return the found recipes
     */
    public Page<RecipeRequest> search(SearchQuery searchQuery){
        return recipesRepository.findAll(SpecificationSearch.bySearchQuery(searchQuery), PageRequest.of(searchQuery.getPageNumber(),
                searchQuery.getPageSize() == 0 ? 10 : searchQuery.getPageSize())
        ).map(this::mapTo);
    }

    private Recipe mapTo(RecipeRequest recipeRequest) {
        return objectMapper.convertValue(recipeRequest, Recipe.class);
    }

    private RecipeRequest mapTo(Recipe recipe) {
        Set<Ingredient> ingredients = recipe.getIngredients();
        recipe.setIngredients(null);
        RecipeRequest recipeRequest = objectMapper.convertValue(recipe, RecipeRequest.class);
        if (!CollectionUtils.isEmpty(ingredients)) {
            recipeRequest.setIngredients(objectMapper.convertValue(ingredients, new TypeReference<>() { }));
        }
        return recipeRequest;
    }

}
