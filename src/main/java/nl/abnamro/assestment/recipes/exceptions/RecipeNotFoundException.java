package nl.abnamro.assestment.recipes.exceptions;

public class RecipeNotFoundException extends RecipesApiException {

    public RecipeNotFoundException(final String message) {
        super(message);
    }

}
