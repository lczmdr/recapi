package nl.abnamro.assestment.recipes.exceptions;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
@Schema(name = "ServiceError", description = "Describes errors happened in the service")
public class ServiceError {

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    @Schema(name = "Time", description = "Error timestamp")
    private Date time;

    @Schema(name = "Message", description = "The error message", example = "Invalid service input")
    private String message;
}
