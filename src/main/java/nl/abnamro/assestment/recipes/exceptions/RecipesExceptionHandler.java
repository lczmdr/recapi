package nl.abnamro.assestment.recipes.exceptions;

import java.util.Date;

import javax.persistence.EntityNotFoundException;

import nl.abnamro.assestment.recipes.exceptions.RecipesApiException;
import nl.abnamro.assestment.recipes.exceptions.ServiceError;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class RecipesExceptionHandler {

    @ExceptionHandler(value = { RecipesApiException.class})
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public ServiceError apiException(RecipesApiException ex) {
        return ServiceError.builder()
                .message(ex.getMessage())
                .time(new Date())
                .build();
    }

    @ExceptionHandler(value = { RecipeNotFoundException.class})
    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    public ServiceError recipeNotFoundException(RecipesApiException ex) {
        return ServiceError.builder()
                .message(ex.getMessage())
                .time(new Date())
                .build();
    }

    @ExceptionHandler(value = {Exception.class})
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public ServiceError genericException(Exception ex) {
        return ServiceError.builder()
                .message("Unexpected Error: " + ex.getMessage())
                .time(new Date())
                .build();
    }

}
