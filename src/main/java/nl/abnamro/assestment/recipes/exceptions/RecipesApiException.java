package nl.abnamro.assestment.recipes.exceptions;

public class RecipesApiException extends RuntimeException{

    public RecipesApiException(String message) {
        super(message);
    }

}
