package nl.abnamro.assestment.recipes.model.search;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class JoinColumnProps {
    @Schema(description = "child entity property name", example = "description")
    private String joinColumnName;
    private SearchFilter searchFilter;
}
