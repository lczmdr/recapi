package nl.abnamro.assestment.recipes.model.request;

import java.util.List;

import nl.abnamro.assestment.recipes.model.DishType;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RecipeRequest {

    @Schema(accessMode = Schema.AccessMode.READ_ONLY)
    private Long id;

    @Schema(description = "Recipe title", example = "Double Cheeseburger")
    private String title;

    @Schema(description = "Recipe Author", example = "John Rambo")
    private String author;

    @Schema(description = "Dish Type", example = "VEGAN", format = "enum")
    private DishType type;

    @Schema(description = "Number of Servings", example = "4")
    private Integer numberOfServings;

    @Schema(description = "Guide to cook the recipe", example = "Preheat a pan at 200 degrees C, spread butter and smash a 200 grams burguer. Flip each side after 2 minutes and put chedar cheese on it")
    private String instructions;

    @Schema(name="ingredients", description = "Recipe Ingredients", example = "[\n        {\n          \"id\": 4,\n          \"description\": \"Cheese\",\n          \"quantity\": \"1\"\n        }\n      ]")
    private List<IngredientRequest> ingredients;

}
