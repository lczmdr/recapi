package nl.abnamro.assestment.recipes.model.search;

import java.util.List;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SearchQuery {
    private List<SearchFilter> searchFilters;
    @Schema(description = "page number", example = "0")
    private int pageNumber;
    @Schema(description = "page size", example = "10")
    private int pageSize;
    @Schema(hidden = true)
    private SortOrder sortOrder;
    private List<JoinColumnProps> joinColumnProps;
}
