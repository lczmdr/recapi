package nl.abnamro.assestment.recipes.model;

public enum DishType {

    VEGAN("VEGAN"),

    VEGETARIAN("VEGETARIAN"),

    REGULAR("REGULAR");

    private final String description;

    DishType(String description) {
        this.description = description;
    }

}
