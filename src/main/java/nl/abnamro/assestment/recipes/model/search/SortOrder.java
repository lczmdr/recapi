package nl.abnamro.assestment.recipes.model.search;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class SortOrder {

    private List<String> ascendingOrder;

    private List<String> descendingOrder;
}
