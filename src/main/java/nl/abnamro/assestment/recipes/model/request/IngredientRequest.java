package nl.abnamro.assestment.recipes.model.request;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class IngredientRequest {

    @Schema(accessMode = Schema.AccessMode.READ_ONLY)
    private Long id;

    @Schema(description = "Ingredient description", example = "Tomatoes")
    private String description;

    @Schema(description = "Ingredient quantity", example = "2")
    private String quantity;

}
