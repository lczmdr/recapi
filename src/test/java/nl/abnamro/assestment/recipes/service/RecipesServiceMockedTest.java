package nl.abnamro.assestment.recipes.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityNotFoundException;

import nl.abnamro.assestment.recipes.model.DishType;
import nl.abnamro.assestment.recipes.model.entity.Recipe;
import nl.abnamro.assestment.recipes.model.request.IngredientRequest;
import nl.abnamro.assestment.recipes.model.request.RecipeRequest;
import nl.abnamro.assestment.recipes.model.search.QueryOperator;
import nl.abnamro.assestment.recipes.model.search.SearchFilter;
import nl.abnamro.assestment.recipes.model.search.SearchQuery;
import nl.abnamro.assestment.recipes.repositories.IngredientsRepository;
import nl.abnamro.assestment.recipes.repositories.RecipesRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

@ExtendWith(MockitoExtension.class)
class RecipesServiceMockedTest {

    @Mock
    private RecipesRepository recipesRepository;

    @Mock
    private IngredientsRepository ingredientsRepository;

    private ObjectMapper objectMapper;

    private RecipesService recipesService;

    private Recipe recipeEntity;

    private RecipeRequest recipeRequest;
    
    private Long recipeId = 1L;

    @BeforeEach
    void setUp(){
        objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        recipeRequest = buildRecipeRequest();
        recipeEntity = objectMapper.convertValue(recipeRequest, Recipe.class);
        recipeEntity.setId(recipeId);
        recipesService = new RecipesService(objectMapper, recipesRepository, ingredientsRepository);
    }

    @Test
    void testCreateRecipe() {
        Mockito.when(recipesRepository.save(ArgumentMatchers.any(Recipe.class))).thenReturn(recipeEntity);
        RecipeRequest recipeDto = recipesService.create(buildRecipeRequest());
        assertTrue(recipeDto.getId() > 0);
    }

    @Test
    void testGetRecipe() {
        Mockito.when(recipesRepository.findById(ArgumentMatchers.any())).thenReturn(Optional.of(recipeEntity));
        RecipeRequest fetchedRecipe = recipesService.getRecipe(recipeId);
        assertEquals(fetchedRecipe.getId(), recipeId);
    }

    @Test
    void testGetRecipeThrownExceptionForNonExistingEntity() {
        Mockito.when(recipesRepository.findById(ArgumentMatchers.any())).thenReturn(Optional.empty());
        EntityNotFoundException exception = assertThrows(EntityNotFoundException.class,
                () -> recipesService.getRecipe(recipeId));
        assertTrue(exception.getMessage().contains(recipeId + ""));
    }

    @Test
    void testUpdateRecipe() {
        String newAuthor = "Mystical Author";
        recipeEntity.setAuthor(newAuthor);
        Mockito.when(recipesRepository.findById(ArgumentMatchers.any())).thenReturn(Optional.of(recipeEntity));
        Mockito.when(recipesRepository.save(ArgumentMatchers.any(Recipe.class))).thenReturn(recipeEntity);

        RecipeRequest updatedRecipe =  recipesService.updateRecipe(recipeRequest,recipeId);
        assertEquals(updatedRecipe.getAuthor(), newAuthor);
    }

    @Test
    void testDeleteRecipe() {
        Mockito.when(recipesRepository.findById(ArgumentMatchers.any())).thenReturn(Optional.of(recipeEntity));
        recipesService.delete(recipeId);
        Mockito.when(recipesRepository.findById(ArgumentMatchers.any())).thenReturn(Optional.empty());
        EntityNotFoundException exception = assertThrows(EntityNotFoundException.class,
                () -> recipesService.getRecipe(recipeId));
        assertTrue(exception.getMessage().contains(recipeId + ""));
    }

    @Test
    void testSearch(){
        Mockito.when(recipesRepository.findAll(ArgumentMatchers.any(Specification.class), ArgumentMatchers.any(Pageable.class)))
                .thenReturn(new PageImpl<>(List.of(recipeEntity)));
        SearchQuery searchQuery = SearchQuery.builder()
                .pageSize(10)
                .pageNumber(0)
                .searchFilters(List.of(SearchFilter.builder()
                        .columnName("author")
                        .operator(QueryOperator.IN)
                        .value(List.of(recipeEntity.getAuthor()))
                        .build()))
                .build();
        Page<RecipeRequest> recipeDtoList = recipesService.search(searchQuery);
        assertTrue(recipeDtoList.getSize()>=1);
    }

    private RecipeRequest buildRecipeRequest() {
        RecipeRequest recipeRequest = new RecipeRequest();
        recipeRequest.setId(1L);
        recipeRequest.setTitle("Cheeseburger");
        recipeRequest.setAuthor("John");
        recipeRequest.setInstructions("Preheat pan");
        recipeRequest.setType(DishType.REGULAR);
        recipeRequest.setNumberOfServings(1);

        IngredientRequest ingredientRequest = new IngredientRequest();
        ingredientRequest.setId(0L);
        ingredientRequest.setDescription("Cheese");
        ingredientRequest.setQuantity("1");

        recipeRequest.setIngredients(List.of(ingredientRequest));

        return recipeRequest;
    }

}
