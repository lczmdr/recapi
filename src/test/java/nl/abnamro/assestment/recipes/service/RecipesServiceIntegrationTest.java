package nl.abnamro.assestment.recipes.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import javax.persistence.EntityNotFoundException;

import nl.abnamro.assestment.recipes.model.DishType;
import nl.abnamro.assestment.recipes.model.request.IngredientRequest;
import nl.abnamro.assestment.recipes.model.request.RecipeRequest;
import nl.abnamro.assestment.recipes.model.search.QueryOperator;
import nl.abnamro.assestment.recipes.model.search.SearchFilter;
import nl.abnamro.assestment.recipes.model.search.SearchQuery;
import nl.abnamro.assestment.recipes.repositories.RecipesRepository;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

@SpringBootTest
class RecipesServiceIntegrationTest {

    @Autowired
    private RecipesService recipesService;

    @Autowired
    private RecipesRepository recipesRepository;

    @Test
    void should_create_recipe() {
        RecipeRequest recipeDto = recipesService.create(buildRecipeRequest());
        assertTrue(recipeDto.getId() > 0);
    }

    @Test
    void should_return_existing_recipe() {
        recipesRepository.findAll(PageRequest.of(0, 1)).get().findFirst()
                .ifPresent(recipe -> {
                    RecipeRequest recipeRequest = recipesService.getRecipe(recipe.getId());
                    assertEquals(recipeRequest.getId(), recipe.getId());
                });
    }

    @Test
    void should_throw_exception_non_existing_recipe() {
        Long entityId = -1L;
        EntityNotFoundException exception = assertThrows(EntityNotFoundException.class, () -> recipesService.getRecipe(entityId));
        assertTrue(exception.getMessage().contains(entityId + ""));
    }

    @Test
    void should_update_existing_recipe() {
        recipesRepository.findAll(PageRequest.of(0, 1)).get().findFirst()
                .ifPresent(recipe -> {
                    RecipeRequest recipeRequest = buildRecipeRequest();
                    recipeRequest.setAuthor(recipeRequest.getAuthor() + " RAMBO");
                    RecipeRequest updatedRecipeRequest =  recipesService.updateRecipe(recipeRequest, recipe.getId());
                    assertEquals(updatedRecipeRequest.getAuthor(), recipeRequest.getAuthor());
                });
    }

    @Test
    void should_delete_existing_recipe() {
        recipesRepository.findAll(PageRequest.of(0, 1)).get().findFirst()
                .ifPresent(recipe -> {
                    recipesService.delete(recipe.getId());
                    EntityNotFoundException exception = assertThrows(EntityNotFoundException.class, () -> recipesService.getRecipe(recipe.getId()));
                    assertTrue(exception.getMessage().contains(recipe.getId() + ""));
                });
    }

    @Test
    void should_throw_exception_when_delete_non_existing_recipe() {
        Long entityId = -1L;
        EntityNotFoundException exception = assertThrows(EntityNotFoundException.class, () -> recipesService.delete(entityId));
        assertTrue(exception.getMessage().contains(entityId + ""));
    }

    @Test
    void should_return_at_least_one_record_with_existing_column_value(){
        recipesRepository.findAll(PageRequest.of(0, 1)).get().findFirst()
                .ifPresent(recipe -> {
                    SearchQuery searchQuery = SearchQuery.builder()
                            .searchFilters(List.of(SearchFilter.builder()
                                    .columnName("author")
                                    .operator(QueryOperator.IN)
                                    .value(List.of(recipe.getAuthor()))
                                    .build()))
                            .build();
                    Page<RecipeRequest> recipeDtoList = recipesService.search(searchQuery);
                    assertTrue(recipeDtoList.getSize()>=1);
                });
    }

    private RecipeRequest buildRecipeRequest() {
        RecipeRequest recipeRequest = new RecipeRequest();
        recipeRequest.setId(1L);
        recipeRequest.setTitle("Cheeseburger");
        recipeRequest.setAuthor("John");
        recipeRequest.setInstructions("Preheat pan");
        recipeRequest.setType(DishType.REGULAR);
        recipeRequest.setNumberOfServings(1);

        IngredientRequest ingredientRequest = new IngredientRequest();
        ingredientRequest.setId(0L);
        ingredientRequest.setDescription("Cheese");
        ingredientRequest.setQuantity("1");

        recipeRequest.setIngredients(List.of(ingredientRequest));

        return recipeRequest;
    }
}
