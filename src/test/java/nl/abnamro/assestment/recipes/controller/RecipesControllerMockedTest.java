package nl.abnamro.assestment.recipes.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import nl.abnamro.assestment.recipes.model.DishType;
import nl.abnamro.assestment.recipes.model.entity.Ingredient;
import nl.abnamro.assestment.recipes.model.request.IngredientRequest;
import nl.abnamro.assestment.recipes.model.request.RecipeRequest;
import nl.abnamro.assestment.recipes.service.RecipesService;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;

@AutoConfigureMockMvc
@WebMvcTest(controllers = RecipesController.class)
public class RecipesControllerMockedTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private RecipesService recipesService;

    @Test
    void returns_201_with_a_valid_input() throws Exception {
        RecipeRequest recipeDto = buildRecipeRequest();
        mockMvc.perform(post("/recipes")
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(recipeDto)))
                .andExpect(status().isCreated());
    }

    @Test
    void returns_200_with_a_valid_recipe_id() throws Exception {
        mockMvc.perform(get("/recipes/{recipeId}", 1)
                        .contentType("application/json"))
                .andExpect(status().isOk());
    }

    @Test
    void returns_200_when_returns_all_recipes() throws Exception {
        mockMvc.perform(get("/recipes")
                        .param("pageSize","10")
                        .param("pageNumber","0")
                        .contentType("application/json"))
                .andExpect(status().isOk());
    }

    @Test
    void return_200_when_updates_a_recipe() throws Exception {
        RecipeRequest recipeDto = buildRecipeRequest();
        mockMvc.perform(get("/recipes/{recipeId}", 1)
                        .content(objectMapper.writeValueAsString(recipeDto))
                        .contentType("application/json"))
                .andExpect(status().isOk());
    }

    @Test
    void returns_204_when_deletes_a_recipe() throws Exception {
        mockMvc.perform(delete("/recipes/{recipeId}", 1)
                        .contentType("application/json"))
                .andExpect(status().isNoContent());
    }

    /**
     * Builds an example recipe request.
     * @return the recipe request
     */
    private RecipeRequest buildRecipeRequest() {
        RecipeRequest recipeRequest = new RecipeRequest();
        recipeRequest.setId(1L);
        recipeRequest.setTitle("Cheeseburger");
        recipeRequest.setAuthor("John");
        recipeRequest.setInstructions("Preheat pan");
        recipeRequest.setType(DishType.REGULAR);
        recipeRequest.setNumberOfServings(1);

        IngredientRequest ingredientRequest = new IngredientRequest();
        ingredientRequest.setId(0L);
        ingredientRequest.setDescription("Cheese");
        ingredientRequest.setQuantity("1");

        recipeRequest.setIngredients(List.of(ingredientRequest));

        return recipeRequest;
    }

}
