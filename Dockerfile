# Start with a base image containing Java runtime
FROM gradle:jdk11 as builder

COPY --chown=gradle:gradle . /home/gradle/src
WORKDIR /home/gradle/src
RUN ./gradlew build -x test

FROM openjdk:11-jre-slim

EXPOSE 8080
COPY --from=builder /home/gradle/src/build/libs/recipes-api-0.0.1-SNAPSHOT.jar /app/recipes-api.jar
WORKDIR /app

ENTRYPOINT ["java","-Dspring.profiles.active=prod","-jar", "/app/recipes-api.jar"]
